#!/usr/bin/env python3 
import os
import argparse
from numpy.lib.type_check import imag
import torch
import torchvision
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data
import torchvision.transforms as transforms

from utils import test_attack,imshow,classes,plot_accuracies
from networks import MobileNetV2,Net
import attacks



use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")

valid_size = 1024 
batch_size = 64
n_epochs = 10
model_file="models/default_model.pth" 
adv_model_file="models/adv_model.pth" 

'''
        Network 02 : MobileNet V2 (from https://github.com/kuangliu/)
'''

class Block(nn.Module):
    '''expand + depthwise + pointwise'''
    def __init__(self, in_planes, out_planes, expansion, stride):
        super(Block, self).__init__()
        self.stride = stride

        planes = expansion * in_planes
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride, padding=1, groups=planes, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, out_planes, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn3 = nn.BatchNorm2d(out_planes)

        self.shortcut = nn.Sequential()
        if stride == 1 and in_planes != out_planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=1, padding=0, bias=False),
                nn.BatchNorm2d(out_planes),
            )

    def forward(self, x):
        out = F.relu(self.bn1(self.conv1(x)))
        out = F.relu(self.bn2(self.conv2(out)))
        out = self.bn3(self.conv3(out))
        out = out + self.shortcut(x) if self.stride==1 else out
        return out


class Net(nn.Module):
    # (expansion, out_planes, num_blocks, stride)
    cfg = [(1,  16, 1, 1),
           (6,  24, 2, 1),  # NOTE: change stride 2 -> 1 for CIFAR10
           (6,  32, 3, 2),
           (6,  64, 4, 2),
           (6,  96, 3, 1),
           (6, 160, 3, 2),
           (6, 320, 1, 1)]
    model_file="models/default_model.pth" 

    def __init__(self, num_classes=10):
        super(Net, self).__init__()
        # NOTE: change conv1 stride 2 -> 1 for CIFAR10
        self.conv1 = nn.Conv2d(3, 32, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(32)
        self.layers = self._make_layers(in_planes=32)
        self.conv2 = nn.Conv2d(320, 1280, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn2 = nn.BatchNorm2d(1280)
        self.linear = nn.Linear(1280, num_classes)

    def _make_layers(self, in_planes):
        layers = []
        for expansion, out_planes, num_blocks, stride in self.cfg:
            strides = [stride] + [1]*(num_blocks-1)
            for stride in strides:
                layers.append(Block(in_planes, out_planes, expansion, stride))
                in_planes = out_planes
        return nn.Sequential(*layers)

    def forward(self, x):
        out = F.relu(self.bn1(self.conv1(x)))
        out = self.layers(out)
        out = F.relu(self.bn2(self.conv2(out)))
        # NOTE: change pooling kernel_size 7 -> 4 for CIFAR10
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)
        out = self.linear(out)
        out = F.log_softmax(out, dim=1)
        return out

    def save(self, model_file):
        '''Helper function, use it to save the model weights after training.'''
        torch.save(self.state_dict(), model_file)

    def load(self, model_file):
        self.load_state_dict(torch.load(model_file, map_location=torch.device(device)))

        
    def load_for_testing(self, project_dir='./'):
        '''This function will be called automatically before testing your
           project, and will load the model weights from the file
           specify in Net.model_file.
           
           You must not change the prototype of this function. You may
           add extra code in its body if you feel it is necessary, but
           beware that paths of files used in this function should be
           refered relative to the root of your project directory.
        '''        
        self.load(os.path.join(project_dir, Net.model_file))


def train_model(net, train_loader,pth_filename, num_epochs=n_epochs):
    print("Starting training")
    criterion = nn.NLLLoss()
    optimizer = optim.Adam(net.parameters(),lr=0.001)

    for epoch in range(num_epochs):  # loop over the dataset multiple times

        running_loss = 0.0
        correct , total = 2*(0,)
        for i, data in enumerate(train_loader, 0):
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data[0].to(device), data[1].to(device)

            # foreward pass
            outputs = net(inputs)
            loss = criterion(outputs,labels)

            # backward pass
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

            '''
                    Validation 
                    --- val_loss : {val_loss:.3f} --- val_acc : {val_correct/val_total:.3f}
            '''
            '''
            val_loss = 0.0
            val_correct , val_total = 2*(0,)

            net.eval()     # Optional when not using Model Specific layer
            for inputs, labels in val_loader:
                inputs, labels = inputs.to(device),labels.to(device)
                outputs = net(data)
                loss = criterion(outputs,labels)
                val_loss = loss.item() * inputs.size(0)
                _, val_predicted = torch.max(outputs.data, 1)
                val_total += labels.size(0)
                val_correct += (val_predicted == labels).sum().item()
            '''
        print(f"epoch [{epoch+1}/{num_epochs}] --- loss : {loss.item():.3f} --- acc : {correct/total:.3f}" )

    net.save(pth_filename)
    print(f'Model saved in {pth_filename}')

def train_adversarial(net, train_loader, pth_filename, num_epochs,atk='fgsm',eps=0.3):

    print("Start training Adversarial...")

    criterion = nn.NLLLoss()
    optimizer = optim.Adam(net.parameters(),lr=0.001)
    attack = getattr(attacks, f"{atk}_attack")
    correct , total , adv_correct , adv_total , step = 5*(0,)
    for epoch in range(num_epochs):  # loop over the dataset multiple times

        running_loss = 0.0
        for i, data in enumerate(train_loader, 0):

            '''
                I)- Natural Training
            '''
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data[0].to(device), data[1].to(device)

            # foreward pass
            optimizer.zero_grad()
            outputs = net(inputs)
            loss = criterion(outputs,labels)

            # backward pass
            loss.backward()
            optimizer.step()
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

            '''
                II)- Adverasarial Training
            '''
            adv_inputs = attack(net,criterion,inputs,labels,eps=eps)
            # foreward pass
            optimizer.zero_grad()
            outputs = net(adv_inputs)
            loss = criterion(outputs,labels)

            # backward pass
            loss.backward()
            optimizer.step()
            _, predicted = torch.max(outputs.data, 1)
            adv_total += labels.size(0)
            adv_correct += (predicted == labels).sum().item()

            '''
                III)- Visualize Accuracy             
            '''
            step += 1
            if step % 500 == 0:
                print(f"[{step}] Natural Training accuracy: [{correct/total:.4f} %]")
                correct , total = 2*(0,)
                print(f"[{step}] Adverasrial Training accuracy: [{adv_correct/adv_total:.4f} %]")
                adv_correct , adv_total = 2*(0,)




        print(f"epoch [{epoch+1}/{num_epochs}] --- loss : {loss.item():.6f}")

    net.save(pth_filename)
    print(f'ADV Model saved in {pth_filename}')

def test_natural(net, test_loader):
    '''Basic testing function.'''
    correct = 0
    total = 0
    # since we're not training, we don't need to calculate the gradients for our outputs
    with torch.no_grad():
        for i,data in enumerate(test_loader, 0):
            images, labels = data[0].to(device), data[1].to(device)
            # calculate outputs by running images through the network
            outputs = net(images)
            # the class with the highest energy is what we choose as prediction
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    return 100 * correct / total

def get_train_loader(dataset, valid_size=1024, batch_size=32):
    '''Split dataset into [train:valid] and return a DataLoader for the training part.'''

    indices = list(range(len(dataset)))
    train_sampler = torch.utils.data.SubsetRandomSampler(indices[valid_size:])
    train = torch.utils.data.DataLoader(dataset, num_workers = 8 ,sampler=train_sampler, batch_size=batch_size)

    return train

def get_validation_loader(dataset, valid_size=1024, batch_size=32):
    '''Split dataset into [train:valid] and return a DataLoader for the validation part.'''

    indices = list(range(len(dataset)))
    valid_sampler = torch.utils.data.SubsetRandomSampler(indices[:valid_size])
    valid = torch.utils.data.DataLoader(dataset, num_workers = 8, sampler=valid_sampler, batch_size=batch_size)

    return valid

def main():

    #### Parse command line arguments 
    parser = argparse.ArgumentParser()
    parser.add_argument("--model-file", default=Net.model_file,
                        help="Name of the file used to load or to sore the model weights."\
                        "If the file exists, the weights will be load from it."\
                        "If the file doesn't exists, or if --force-train is set, training will be performed, "\
                        "and the model weights will be stored in this file."\
                        "Warning: "+Net.model_file+" will be used for testing (see load_for_testing()).")
    parser.add_argument('-f', '--force-train', action="store_true",
                        help="Force training even if model file already exists"\
                             "Warning: previous model file will be erased!).")
    parser.add_argument('-e', '--num-epochs', type=int, default=n_epochs,
                        help="Set the number of epochs during training")
    args = parser.parse_args()

    #### Create model and move it to whatever device is available (gpu/cpu)
    net = Net()
    net.to(device)

    #### Model training (if necessary)
    if not os.path.exists(args.model_file) or args.force_train:
        print("Training model")
        print(args.model_file)

        train_transform = transforms.Compose([transforms.ToTensor()]) 
        cifar = torchvision.datasets.CIFAR10('./data/', download=True, transform=train_transform)
        train_loader = get_train_loader(cifar, valid_size, batch_size=batch_size)
        train_model(net, train_loader, args.model_file, args.num_epochs)
        print("Model save to '{}'.".format(args.model_file))

    #### Model testing
    print("Testing with model from '{}'. ".format(args.model_file))

    # Note: You should not change the transform applied to the
    # validation dataset since, it will be the only transform used
    # during final testing.
    cifar = torchvision.datasets.CIFAR10('./data/', download=True, transform=transforms.ToTensor()) 
    valid_loader = get_validation_loader(cifar, valid_size)

    net.load(args.model_file)

    acc = test_natural(net, valid_loader)
    print("Model natural accuracy (valid): {}".format(acc))

    if args.model_file != Net.model_file:
        print("Warning: '{0}' is not the default model file, "\
              "it will not be the one used for testing your project. "\
              "If this is your best model, "\
              "you should rename/link '{0}' to '{1}'.".format(args.model_file, Net.model_file))

def test_project(net,train_natural=0,train_adv=1,atk='fgsm'):

    #### Create model and move it to whatever device is available (gpu/cpu)
    net.to(device)

    if train_natural:
        #### Natural Model training
        print("Natural Training model")
        print(model_file)

        train_transform = transforms.Compose([transforms.ToTensor()]) 
        cifar = torchvision.datasets.CIFAR10('./data/', download=True, transform=train_transform)
        train_loader = get_train_loader(cifar, valid_size, batch_size=batch_size)
        train_model(net, train_loader, model_file, n_epochs)
        print("Model saved to '{}'.".format(model_file))

    #### Natural Model testing
    print("Testing with model from '{}'. ".format(model_file))

    cifar = torchvision.datasets.CIFAR10('./data/', download=True, transform=transforms.ToTensor()) 
    valid_loader = get_validation_loader(cifar, valid_size)

    net.load(net.model_file)

    acc = test_natural(net, valid_loader)
    print("Model natural accuracy (valid): {}".format(acc))

    '''
        II)- Adversarial Training
    '''
    #### Create model and move it to whatever device is available (gpu/cpu)
    adv_net = net
    adv_net.to(device)

    if train_adv:
        #### Natural Model training
        print(f"Adversarial [{atk.upper()}] Training model")
        print(adv_model_file)

        train_transform = transforms.Compose([transforms.ToTensor()]) 
        cifar = torchvision.datasets.CIFAR10('./data/', download=True, transform=train_transform)
        train_loader = get_train_loader(cifar, valid_size, batch_size=batch_size)
        train_adversarial(adv_net, train_loader, adv_model_file, n_epochs,atk=atk)
        print("Model saved to '{}'.".format(adv_model_file))

    #### Adverasial Model testing
    print("Testing with model from '{}'. ".format(adv_model_file))

    cifar = torchvision.datasets.CIFAR10('./data/', download=True, transform=transforms.ToTensor()) 
    valid_loader = get_validation_loader(cifar, valid_size)

    adv_net.load(adv_model_file)

    adv_acc = test_natural(adv_net, valid_loader)
    print("Model Adversarial accuracy (valid): {}".format(adv_acc))


def train_step(net,optimizer,criterion,inputs,labels):
    # foreward pass
    optimizer.zero_grad()
    outputs = net(inputs)
    loss = criterion(outputs,labels)

    # backward pass
    loss.backward()
    optimizer.step()
    return loss



def custom_main():
    net = Net()
    net.to(device)
    net.load(model_file)

    cifar = torchvision.datasets.CIFAR10('./data/', download=True, transform=transforms.ToTensor()) 
    valid_loader = get_validation_loader(cifar, valid_size)
    valid_iter = iter(valid_loader)

    '''
    for images,labels in valid_loader:
       img, lbl = next(valid_iter)
       images , labels = images.to(device),labels.to(device)
       
       outputs = net(images)
       imshow(torchvision.utils.make_grid(img))
       print('Label : ',' '.join('%5s' % classes[lbl[j]] for j in range(1)))
       _ , pred = torch.max(outputs.data, 1)
       print(f"Prediction : {classes[pred]}")
    '''
    epsilons = np.linspace(0,1,20)
    _acc = test_attack(net, valid_loader,epsilons=epsilons,atk='pgd')
    print("Model Attacked accuracy (valid): {}".format(_acc))

    plot_accuracies(epsilons,_acc)












if __name__ == "__main__":
    #test_project(net=MobileNetV2(),atk='fgsm')
    main()
