# Install in GRid 5000 : pip install torch==1.9.0+cu111 torchvision==0.10.0+cu111 torchaudio==0.9.0 -f https://download.pytorch.org/whl/torch_stable.html

import numpy as np
import pickle
import matplotlib.pyplot as plt
import torch
from torch.optim import optimizer
from torch.utils.data import dataloader
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from model import *
from networks import *
from attacks import *
from utils import *

np.random.seed(42)
torch.manual_seed(42)


#################### Params ############################
use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")

valid_size = 1024
batch_size = 64
n_epochs = 20
lr = 0.001
########################################################





def get_data():
    train_transform = transforms.Compose([transforms.ToTensor()]) 
    cifar = torchvision.datasets.CIFAR10('./data/', download=True, transform=train_transform)
    train_loader = get_train_loader(cifar, valid_size, batch_size=batch_size)
    val_loader = get_validation_loader(cifar, valid_size)
    data_loader = {'train':train_loader,'val':val_loader}
    return data_loader

def get_model(name='MobileNetV2'):
    # Model 
    model = Net().to(device) if name == 'Net' else MobileNetV2().to(device)
    optimizer = optim.Adam(model.parameters(),lr=lr, betas=(0.9, 0.999))
    criterion = nn.NLLLoss()
    return model,optimizer,criterion

def test_project(test_loader,name='MobileNetV2',pth='models/model.pth'):
    # Model 
    model = Net().to(device) if name == 'Net' else MobileNetV2().to(device)
    model.load(pth)
    acc = test_natural(model, test_loader)
    print(f"Model {name} natural accuracy (test): {acc}%")

def save_history(pth='models/history',history={}):
    with open(pth, 'wb') as f_hist:
        pickle.dump(history, f_hist)

def main():
    #### Parse command line arguments 
    parser = argparse.ArgumentParser()
    parser.add_argument("-n",'--net', action="store_true")
    parser.add_argument('-m','--mobnet', action="store_true")
    args = parser.parse_args()

    model_name = 'MobileNetV2' if args.mobnet else 'Net'
    print(f" Model : {model_name}")

    data_loader = get_data()
    model , optimizer , criterion = get_model(name=model_name)
    model.load('models/default_model.pth')
    loss,val_loss,acc,val_acc = adv_train(model,optimizer,criterion,data_loader,pth=f"models/model_{model_name}.pth",epochs=n_epochs)

    results = {'loss':loss,'val_loss':val_loss,'acc':acc,'val_acc':val_acc} 
    #print(f" Losses : {loss}")
    #print(f" Val_losses : {val_loss}")
    #print(f" Acc : {acc}")
    #print(f" Val_acc : {val_acc}")

    test_project(data_loader['val'],name=model_name)

    save_history(pth=f"models/hist_{model_name}",history=results)

    return 0

if __name__ == '__main__':
    main()
