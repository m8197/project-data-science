import torch
import torch.nn.functional as F

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")

def fgsm_attack(net, loss, images, labels, eps=1e-1) :
    
    images,labels = images.to(device),labels.to(device)
    images.requires_grad = True

    outputs = net(images)
    
    net.zero_grad()
    cost = loss(outputs, labels).to(device)
    cost.backward()
    
    attack_images = images + eps*images.grad.sign()
    attack_images = torch.clamp(attack_images, 0, 1)
    
    return attack_images

def pgd_attack(net,loss,images,labels,n_iter=40,eps=0.03,alpha=2/255,norm='inf'):

    images,labels = images.to(device),labels.to(device)
    adv_img = images.clone().detach().to(device)
    natural_images = images.data

    for _ in range(n_iter):
        adv_img.requires_grad = True
        outputs = net(adv_img)

        #net.zero_grad()
        cost = loss(outputs, labels).to(device)
        cost.backward()

        adv_img.data = adv_img + alpha*torch.sign(adv_img.grad)
        if norm == 'inf':
            adv_img.adv = torch.max(torch.min(adv_img, images + eps), images - eps)

        #eta = torch.clamp(attack_images - natural_images, min=-eps, max=eps)
        adv_img.data = torch.clamp(adv_img, min=0, max=1)

    return adv_img


def pgd(model, x, y, criterion, n_steps, step_size, step_norm, eps, eps_norm,clamp=(0,1), y_target=None):
    """Performs the projected gradient descent attack on a batch of images."""
    x_adv = x.clone().detach().requires_grad_(True).to(device)
    targeted = y_target is not None
    num_channels = x.shape[1]

    for i in range(n_steps):
        _x_adv = x_adv.clone().detach().requires_grad_(True).to(device)

        prediction = model(_x_adv).to(device)
        loss = criterion(prediction, y_target if targeted else y)
        loss.backward()

        with torch.no_grad():
            if step_norm == 'inf':
                gradients = _x_adv.grad.sign() * step_size
            else:
                gradients = _x_adv.grad * step_size / _x_adv.grad.view(_x_adv.shape[0], -1)\
                    .norm(step_norm, dim=-1)\
                    .view(-1, num_channels, 1, 1)

            if targeted:
                x_adv -= gradients
            else:
                x_adv += gradients

        if eps_norm == 'inf':
            x_adv = torch.max(torch.min(x_adv, x + eps), x - eps)
        else:
            print(f" Norm : {int(eps_norm)}")
            delta = x_adv - x

            mask = delta.view(delta.shape[0], -1).norm(int(eps_norm), dim=1) <= eps

            scaling_factor = delta.view(delta.shape[0], -1).norm(ord=int(eps_norm), dim=1)
            scaling_factor[mask] = eps

            delta *= eps / scaling_factor.view(-1, 1, 1, 1)

            x_adv = x + delta
            
        x_adv = x_adv.clamp(*clamp)

    return x_adv.detach()
