import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import gc
import attacks 



use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")
print(f"Device {device} ")
classes = ('plane', 'car', 'bird', 'cat','deer',
           'dog', 'frog', 'horse', 'ship', 'truck')

def imshow(img):
    fig = plt.figure(figsize = (5, 15))
    img = img / 2 + 0.5
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()

def test_attack(net, test_loader,epsilons=[1e-2],alpha=2/255,atk='fgsm'):
    loss = nn.NLLLoss()
    correct = 0
    total = 0
    _acc = [] 
    attack = getattr(attacks, f"{atk}_attack")

    for eps in epsilons:
      for i,data in enumerate(test_loader, 0):
          images, labels = data[0].to(device), data[1].to(device)
          attacked_images = attack(net,loss,images,labels,eps=eps).to(device)
          outputs = net(attacked_images)
          _, predicted = torch.max(outputs.data, 1)
          total += labels.size(0)
          correct += (predicted == labels).sum().item()
      acc = 100 * correct / total
      _acc.append(acc)

    return _acc

def plot_accuracies(epsilons,acc):
    fig,ax = plt.subplots(1,1)
    fig.set_size_inches((12,5))
    ax.plot(epsilons, acc,'r',label = 'Attacked acc')
    ax.set_title(f'Accuracy Attacked')
    ax.legend()
    fig.show()


def adv_train(model,optimizer,criterion,data_loader,pth='models/model.pth',epsilons=[0.03],norm='inf',steps=40,epochs=20):
    print("Adversarial Training...")
    model.to(device)
    train_losses = val_losses = train_acc = val_accs = []
    total = val_total = loss_i = val_loss_i = acc_i = val_acc_i = 0
    for epoch in range(epochs):
        #total = val_total = loss_i = val_loss_i = acc_i = val_acc_i = 0
        gc.collect()
        for phase in ('train','val'):
            for eps in epsilons:
                #print(f"PGD Attack with eps = {eps}")
                for i , (inputs, labels) in enumerate(data_loader[phase]):
                    inputs , labels  = inputs.to(device),labels.to(device)
                    optimizer.zero_grad()
                    #print(f"[{i+1}] Attack [{len(inputs)}] images...")
                    #x_adv = attacks.pgd_attack(model,criterion,inputs,labels).to(device)
                    #x_adv = TA.PGD(model,eps=0.03)(inputs,labels).to(device)
                    if np.random.randint(0,2):
                        x_adv = attacks.pgd(model, inputs, labels, criterion, 
                                            n_steps=steps, step_size=0.01,
                                            eps=0.5, eps_norm=norm,
                                            step_norm='l2').to(device)
                    else :
                        x_adv = attacks.pgd(model, inputs, labels, criterion, 
                                            n_steps=steps, step_size=0.01,
                                            eps=0.03, eps_norm=norm,
                                            step_norm='inf').to(device)

                    with torch.cuda.amp.autocast():
                         outputs = model(x_adv)
                         _, predicted = torch.max(outputs.data, 1)
                         loss = criterion(outputs, labels)

                    if phase == 'train':
                     #optimizer.zero_grad()
                      loss.backward()
                      optimizer.step()

                      loss_i += loss.item()
                      acc_i += (predicted == labels).sum().item()
                      total += labels.size(0)

                    else:
                        val_loss_i += loss.item()
                        val_acc_i += (predicted == labels).sum().item()
                        val_total += labels.size(0)
                    loss , val_loss = loss_i / len(data_loader['train']) , val_loss_i / len(data_loader['val'])
                    try :
                        acc , val_acc   = acc_i / total , val_acc_i / val_total
                    except ZeroDivisionError:
                           acc = val_acc = 0
        if epoch % 2 == 0:
           model.save(pth)
        print(f"epoch [{epoch+1}/{epochs}] loss : {loss:.4f} --- val_loss : {val_loss:.4f} --- acc : {100*acc:.2f}% --- val_acc : {100*val_acc:.2f}% ")

        train_losses.append(loss)
        val_losses.append(val_loss)
        train_acc.append(acc)
        val_accs.append(val_acc)

    model.save(pth)
    return train_losses,val_losses,train_acc,val_accs
