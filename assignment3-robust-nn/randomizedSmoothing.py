import torch
import numpy as np
from scipy.stats import binom_test
from scipy.stats import norm
from statsmodels.stats.proportion import proportion_confint

ABSTAIN = -1

def sampleUnderNoise(f,x,n,sigma,num_class):
    with torch.no_grad():
        ### Draw n samples of noise eps(1...i...n) ~ N(0,sigma^2.I)
        x_n = x.repeat((n, 1, 1, 1))
        noise = torch.randn_like(x_n)*sigma

        ### Run the noisy images through the base classifier f to obtain the predictions f(x+eps_i)
        pred = f(x_n+noise).argmax(dim = 1)

        ### count for classes
        counts = np.zeros(num_class)
        for i in pred:
            counts[i] += 1
    return counts

def smoothPredict(f,sigma,x,n,alpha,num_class):
    f.eval()
    counts=sampleUnderNoise(f,x,n,sigma,num_class)
    hat_cB, hat_cA = counts.argsort()[-2:]
    nA, nB = counts[hat_cA], counts[hat_cB]
    if binom_test(nA, nA + nB, p=0.5) > alpha:
        return ABSTAIN
    else:
        return hat_cA

def smoothCertify(f,sigma,x,n0,n,alpha,num_class):
    counts0 = sampleUnderNoise(f,x,n0,sigma,num_class)
    hat_cA = counts0.argsort()[-1:]
    counts = sampleUnderNoise(f,x,n,sigma,num_class)
    nA = counts[hat_cA]
    lb_pA = proportion_confint(nA, n, alpha = 2 * alpha, method = 'beta')
    r = sigma * norm.ppf(lb_pA)
    if lb_pA > 0.5:
        return hat_cA, r
    else:
        ABSTAIN
