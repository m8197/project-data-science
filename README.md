# Assignment1 - Optimal Transport

My part :

- [OptimalTransport.py](assignment1-optimal-transport/OptimalTransport.py): Linear Programming for Optimal Transport
- [SinkhornAlgo.py](assignment1-optimal-transport/SinkhornAlgo.py): Function for Sinkhorn Algorithm
- [SinkhornOT.py](assignment1-optimal-transport/SinkhornOT.py): Experiment of Optimal Transport using Sinkhorn Algorithm
- [clean.py](assignment1-optimal-transport/clean.py): Data cleaning and preparation
- [matrix_scaling_and_pred.py](assignment1-optimal-transport/matrix_scaling_and_pred.py): Experiment of Inverse Optimal Transport using Matrix scaling


# Assignment 2 - Autoencoders

My part:

- [VAE_Complete_Notebook.ipynb](assignment2-autoencoder/VAE_Complete_Notebook.ipynb): 
    - function: generate_plan_imgs
    - All the "Latent space" and "Reconstruction and Generation of the images" part

# Assignment 3 - Robust Neural Network

My part:

- [testRamdomizedSmoothing.ipynb](assignment3-robust-nn/testRamdomizedSmoothing.ipynb): Experiments of Ramdomized Smoothing Algorithm
- [randomizedSmoothing.py](assignment3-robust-nn/randomizedSmoothing.py): Functions in Ramdomized Smoothing Algorithm

