import numpy as np

def Sinkhorn(a, b, C, eps, num_iter):
    n = a.shape[0]
    m = b.shape[0]

    K = np.exp(-C/eps)
    v = np.ones((m,1))

    for l in range(num_iter):
        u = a / (K @ v)
        v = b / (K.T @ u)

    u = u.reshape(n,)
    v = v.reshape(m,)
    P = np.diag(u)@K@np.diag(v)

    return P

