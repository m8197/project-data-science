import numpy as np
import matplotlib.pyplot as plt
from sinkhornAlgo import Sinkhorn
from metrics import rmse


def generate_cost(m, n):
    return np.random.rand(m, n)


def shrink(M, tau):
    return np.maximum(M - tau, np.zeros(M.shape))


def svd_threshold(M, tau):
    U, S, V = np.linalg.svd(M, full_matrices=False)
    return U @ np.diag(shrink(S, tau)) @ V


def regularization(mat):
    mat -= np.min(mat)
    mat /= np.max(mat)


def pgd(
        observed_matching,
        step_size,
        lam, epsilon,
        num_iter,
        num_iter_sink,
        verbose=False,
        original_rating=None,
        support=None
):
    m, n = observed_matching.shape
    observed_marginal_users = observed_matching.sum(axis=1)
    observed_marginal_users = observed_marginal_users.reshape((m, 1))
    observed_marginal_items = observed_matching.sum(axis=0)
    observed_marginal_items = observed_marginal_items.reshape((n, 1))

    evaluation_list = []

    c = generate_cost(m, n)
    for i in range(num_iter):
        matching = Sinkhorn(observed_marginal_users, observed_marginal_items, c, epsilon, num_iter_sink)
        c -= step_size * (matching - observed_matching)
        regularization(c)
        c = svd_threshold(c, step_size * lam)
        if verbose:
            rank = np.linalg.matrix_rank(c)
            if support is not None and original_rating is not None:
                evaluation_list.append(rmse(original_rating, matching, support))
                print("iteration n°{} out of {}: rank = {}, rmse = {}".format(i + 1, num_iter, rank, evaluation_list[-1]))
            else:
                print("iteration n°{} out of {}: rank = {}".format(i + 1, num_iter, rank))
    matching = Sinkhorn(observed_marginal_users, observed_marginal_items, c, epsilon, num_iter_sink)

    if verbose and support is not None and original_rating is not None:
        plt.plot(np.arange(0, num_iter), evaluation_list)
        plt.xlabel("number of iterations")
        plt.ylabel("RMSE between the constructed matching and the true matching")
        plt.show()

    return matching, c
