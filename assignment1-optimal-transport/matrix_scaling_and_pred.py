import pandas as pd
import numpy as np

def generate_cost(m, n):
    return np.random.random((m, n))

def has_empty_rows_or_columns(mat):
    sum_rows = mat.sum(axis=0)
    sum_columns = mat.sum(axis=1)
    empty = (sum_rows == 0).any() or (sum_columns == 0).any()
    return empty

def produce_databases(filename):
    df = pd.read_csv(filename)

    dict_movie = {}
    max_key = -1
    for k in range(len(df)):
        movie_id = df.iloc[k, 1]
        if movie_id not in dict_movie:
            max_key += 1
            dict_movie[movie_id] = max_key

    n = np.max(df['userId'])
    m = len(dict_movie)

    pi_hat = 0.0000001*np.ones((n, m))
    for k in range(len(df)):
        user_id = df.iloc[k, 0] - 1
        movie_id = df.iloc[k, 1]
        pi_hat[user_id, dict_movie[movie_id]] = 1

    # generate training matrix pi
    np.random.seed(42)
    choice = np.random.choice(len(df), int(0.2*len(df)), replace=False)

    df_train = df.copy()
    df_train = df_train.drop(choice, axis = 0)

    pi_hat_train = 0.0000001*np.ones((n,m))
    for k in range(len(df_train)):
        user_id = df_train.iloc[k,0] - 1
        movie_id = df_train.iloc[k,1]
        pi_hat_train[user_id, dict_movie[movie_id]] = 1

    return pi_hat, pi_hat_train

def clip(x,gamma):
    clipped = np.minimum(x,gamma)
    clipped = np.maximum(clipped,-gamma)
    return clipped

def prox(c_hat,gamma):
    nc, mc = c_hat.shape
    c = c_hat - clip(c_hat,gamma)
    return c

def Sinkhorn(a, b, C, eps, num_iter):
    n = a.shape[0]
    m = b.shape[0]

    K = np.exp(-C/eps)
    v = np.ones((m,1))

    for l in range(num_iter):
        u = a / (K @ v)
        v = b / (K.T @ u)

    u = u.reshape(n,)
    v = v.reshape(m,)
    P = np.diag(u)@K@np.diag(v)

    return P

if __name__ == "__main__":
    pi_hat, pi_hat_train = produce_databases('/Users/xuanzehui/Downloads/ml-latest-small/ratings.csv')
    normalized_pi_hat = pi_hat/np.sum(pi_hat)
    normalized_pi_hat_train = pi_hat_train/np.sum(pi_hat_train)

    epsilon = 0.1

    observed_marginal_users = normalized_pi_hat_train.sum(axis=1)
    observed_marginal_items = normalized_pi_hat_train.sum(axis=0)
    m, n = normalized_pi_hat_train.shape
    alpha = np.zeros(m)
    beta = np.zeros(n)
    u = np.exp(alpha / epsilon)
    v = np.exp(beta / epsilon)
    c = generate_cost(m, n)
    for i in range(100):
        print("iteration n°{}".format(i))
        K = np.exp(- c / epsilon)
        u, v = observed_marginal_users / (K @ v), observed_marginal_items / (K.T @ u)
        K = normalized_pi_hat_train / np.outer(u, v)
        c_old=c
        c = prox(-epsilon * np.log(K),1)
        print(np.sum(np.abs(c-c_old)))
    alpha = epsilon * np.log(u)
    beta = epsilon * np.log(v)

    # real marginal
    a = np.sum(normalized_pi_hat,axis = 1).reshape(pi_hat.shape[0],1)
    b = np.sum(normalized_pi_hat, axis = 0).reshape(pi_hat.shape[1],1)
    normalized_pi_pred=Sinkhorn(a,b,c, eps = 0.01, num_iter=2000)
    # normalized_pi_pred2=Sinkhorn(a,b,c, eps = 0.001, num_iter=2000)
    # normalized_pi_pred3=Sinkhorn(a,b,c, eps = 0.0001, num_iter=2000)
    N = np.sum(pi_hat)
    pi_pred = normalized_pi_pred*N

    # RMSE for normalized matrix
    RMSE = np.mean((pi_pred-pi_hat)**2)
    RMSE2 = np.mean((pi_pred-normalized_pi_hat_train*N)**2)

