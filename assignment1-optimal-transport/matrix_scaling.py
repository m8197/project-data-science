import numpy as np


def generate_cost(m, n):
    return np.random.random((m, n))


def cost_learning(observed_matching, epsilon):
    observed_matching_modified = observed_matching + 0.01
    observed_marginal_users = observed_matching_modified.sum(axis=1)
    observed_marginal_items = observed_matching_modified.sum(axis=0)
    m, n = observed_matching.shape
    alpha = np.zeros(m)
    beta = np.zeros(n)
    u = np.exp(alpha / epsilon)
    v = np.exp(beta / epsilon)
    c = generate_cost(m, n)
    for i in range(10):
        print("iteration n°{}".format(i))
        K = np.exp(- c / epsilon)
        u, v = observed_marginal_users / (K @ v), observed_marginal_items / (K.T @ u)
        K = observed_matching_modified / np.outer(u, v)
        c = -epsilon * np.log(K)
    alpha = epsilon * np.log(u)
    beta = epsilon * np.log(v)
    pi = np.diag(u) @ K @ np.diag(v)
    return pi, alpha, beta, c
