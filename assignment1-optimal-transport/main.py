import numpy as np
from clean import produce_databases
from matrix_scaling import cost_learning
from proximal_gradient_descent import pgd
from metrics import rmse


filename = ""

true_matching, observed_matching, support = produce_databases(filename)
# pi, _, _, _ = cost_learning(observed_matching, 1)
pi, cost = pgd(observed_matching, 0.1, 0.1, 0.1, 6, 1000, verbose=True, original_rating=true_matching, support=support)

difference = observed_matching - pi
error = true_matching - pi

m, n = observed_matching.shape
nb_rating = int(np.sum(true_matching))
sorted_indices = np.argsort(pi, axis=None)[-nb_rating:]
final_matching = np.zeros(n * m)
final_matching[sorted_indices] = 1
final_matching = final_matching.reshape((m, n))

sorted_indices_cost = np.argsort(cost, axis=None)[:nb_rating]
final_matching_cost = np.zeros(n * m)
final_matching_cost[sorted_indices_cost] = 1
final_matching_cost = final_matching_cost.reshape((m, n))

evaluation = rmse(true_matching, final_matching, support)

evaluation_cost = rmse(true_matching, final_matching_cost, support)

print(evaluation)

print(evaluation_cost)
