import pandas as pd
import numpy as np


def has_empty_rows_or_columns(mat):
    sum_rows = mat.sum(axis=0)
    sum_columns = mat.sum(axis=1)
    empty = (sum_rows == 0).any() or (sum_columns == 0).any()
    return empty


def produce_databases(filename):
    df = pd.read_csv(filename)

    dict_movie = {}
    max_key = -1
    for k in range(len(df)):
        movie_id = df.iloc[k, 1]
        if movie_id not in dict_movie:
            max_key += 1
            dict_movie[movie_id] = max_key

    n = np.max(df['userId'])
    m = len(dict_movie)
    pi_hat = np.zeros((n, m))
    for k in range(len(df)):
        user_id = df.iloc[k, 0] - 1
        movie_id = df.iloc[k, 1]
        pi_hat[user_id, dict_movie[movie_id]] = 1

    # generate training matrix pi
    np.random.seed(42)
    choice = np.random.choice(len(df), int(0.2*len(df)), replace=False)
    df_test = df.iloc[choice]

    support = np.full((n, m), False)
    pi_hat_train = np.copy(pi_hat)

    for k in range(len(df_test)):
        user_id = df_test.iloc[k, 0] - 1
        movie_id = df_test.iloc[k, 1]
        movie_value = dict_movie[movie_id]
        pi_hat_train[user_id, movie_value] = 0
        if has_empty_rows_or_columns(pi_hat_train):
            pi_hat_train[user_id, movie_value] = 1
        else:
            support[user_id, movie_value] = True

    print("fraction suppressed: {}%".format(100 * np.sum(support) / len(df)))

    return pi_hat, pi_hat_train, support
