from scipy.optimize import linprog
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

np.random.seed(42)

# number of mass
n = 10
# generate location of "mass"
x = np.random.normal(0,1,size=(10,2))


# number of hole
m = 15
# generate location of "hole"
y = np.random.normal(1,1,size=(m,2))

# visualization of the location of x and y
plt.figure()
plt.scatter(x[:,0],x[:,1],label='x')
plt.scatter(y[:,0],y[:,1],label='y')
plt.legend()
plt.show()

# calculation of matrix cost C
C=np.zeros((n,m))
for i in range(n):
    for j in range(m):
        C[i,j] = np.sum((x[i]-y[j])**2)

# reshape C
c = C.reshape(n*m,1)

# create A_eq
Id = np.identity(m)
A_eq_1 = np.concatenate((Id,Id),axis=1)
for k in range(n-2):
    A_eq_1 = np.concatenate((A_eq_1,Id),axis=1)

A_eq_2 = np.zeros((n,n*m))
for k in range(n):
    A_eq_2[k,k*m:(k+1)*m] = np.ones((1,m))

A_eq = np.concatenate((A_eq_1,A_eq_2), axis=0)

# create vector l, u and b_eq
l = np.zeros((n*m,1))
val_u = np.min([1/n,1/m])
u = val_u*np.ones((n*m,1))
b_eq_1 = 1/m*np.ones((m,1))
b_eq_2 = 1/n*np.ones((n,1))
b_eq = np.concatenate((b_eq_1,b_eq_2),axis=0)

res = linprog(c=c, A_eq=A_eq, b_eq=b_eq, bounds=(0,val_u))

pi = res.x.reshape(n,m)
print(pi)
np.savetxt('resultOT.txt',pi)

plt.figure(figsize=(5,4))
for i in range(n):
    for j in range(m):
        if pi[i,j]>0.00001:
            plt.arrow(x[i,0],x[i,1],y[j,0]-x[i,0],y[j,1]-x[i,1],width=pi[i,j]*0.5,length_includes_head=True,fc='black',ec='black',alpha=0.7)
plt.scatter(x[:,0],x[:,1],label='x',s=100,c='red')
plt.scatter(y[:,0],y[:,1],label='y',s=100,c='blue')
plt.legend()
plt.show()
