import numpy as np


def rmse(original_rating, estimated_rating, support):
    error = original_rating[support] - estimated_rating[support]
    metric = np.mean(error**2)
    return metric