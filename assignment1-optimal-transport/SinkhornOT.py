from scipy.optimize import linprog
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

np.random.seed(42)

# number of mass
n = 10
# generate location of "mass"
x = np.random.normal(0,1,size=(10,2))


# number of hole
m = 15
# generate location of "hole"
y = np.random.normal(1,1,size=(m,2))

# # visualization of the location of x and y
# plt.figure()
# plt.scatter(x[:,0],x[:,1],label='x')
# plt.scatter(y[:,0],y[:,1],label='y')
# plt.legend()
# plt.show()

# calculation of matrix cost C
C=np.zeros((n,m))
for i in range(n):
    for j in range(m):
        C[i,j] = np.sum((x[i]-y[j])**2)

# marginal distribution a and b
a = 1/n*np.ones((n,1))
b = 1/m*np.ones((m,1))

# set the hyper-parameter eps
eps = 0.1

# Gibbs kernel associated to the cost matrix C
K = np.exp(-C/eps)

# two scaling variable which parameterize the solution
v = np.ones((m,1))

for l in range(5000):
    u = a / (K @ v)
    v = b / (K.T @ u)

# calculation of solution
u = u.reshape(n,)
v = v.reshape(m,)
P = np.diag(u)@K@np.diag(v)

print(P)
np.savetxt('resultSinkhornOT.txt',P)
