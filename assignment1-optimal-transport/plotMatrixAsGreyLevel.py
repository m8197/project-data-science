
import matplotlib.pyplot as plt

def showMatrixAsGreyLevelImage(mat):
    plt.imshow(mat,cmap='gray')
    plt.show()